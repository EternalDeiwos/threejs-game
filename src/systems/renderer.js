/**
 * Dependencies
 * @ignore
 */
import { WebGLRenderer } from 'three'

/**
 * System
 * @ignore
 */
async function createRenderer() {
  const renderer = new WebGLRenderer({ antialias: true })

  // turn on the physically correct lighting model
  renderer.physicallyCorrectLights = true

  return renderer
}

/**
 * Exports
 * @ignore
 */
export default createRenderer
