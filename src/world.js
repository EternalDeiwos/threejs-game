/**
 * Dependencies
 * @ignore
 */
import { Clock } from 'three'

/**
 * Module Dependencies
 * @ignore
 */
import createCamera from './components/camera'
import createScene from './components/scene'
import createCube from './components/cube'
import createLights from './components/lights'
import createRenderer from './systems/renderer'
import createStats from './systems/stats'
import createControls from './systems/controls'

/**
 * Namespaced
 * @ignore
 */
let updatables
let container
let camera
let scene
let renderer
let stats
let clock

/**
 * World
 * @ignore
 */
class World {
  constructor(el) {
    container = el
    updatables = []
    clock = new Clock()
  }

  async init() {
    camera = await createCamera()
    renderer = await createRenderer()
    scene = await createScene()
    stats = await createStats()

    // Add renderer to DOM
    container.append(renderer.domElement)

    // Add stats overlay to DOM
    container.append(stats.dom)

    const controls = await createControls(camera, renderer.domElement)
    const cube = await createCube()
    const light = await createLights()

    cube.rotation.set(-0.5, -0.1, 0.8)

    scene.add(cube, light)
    updatables.push(cube)
    updatables.push(controls)

    // Resize renderer when window size changes
    window.addEventListener('resize', () => {
      this.resizeRenderer()
      this.render()
    })

    // Run resize once at start
    this.resizeRenderer()
  }

  resizeRenderer() {
    // Set the camera's aspect ratio
    camera.aspect = window.innerWidth / window.innerHeight

    // update the camera's frustum
    camera.updateProjectionMatrix()

    // update the size of the renderer AND the canvas
    renderer.setSize(window.innerWidth, window.innerHeight)

    // set the pixel ratio (for mobile devices)
    renderer.setPixelRatio(window.devicePixelRatio)
  }

  render() {
    renderer.render(scene, camera)
  }

  tick() {
    const delta = clock.getDelta()
    let draw = false

    for (const object of updatables) {
      if (object.tick(delta, this)) {
        // Set draw flag if at least one updatable requests it
        draw = true
      }
    }

    stats.update()
    return draw
  }

  start() {
    renderer.setAnimationLoop(() => {
      if (this.tick()) {
        this.render()
      }
    })
  }

  stop() {
    renderer.setAnimationLoop(null)
  }
}

export default World
