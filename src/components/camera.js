/**
 * Dependencies
 * @ignore
 */
import { PerspectiveCamera } from 'three'

/**
 * System
 * @ignore
 */
async function createCamera() {
  const camera = new PerspectiveCamera(
    75, // fov
    1, // aspect ratio
    0.1, // near clipping plane
    100 // far clipping plane
  )

  // Observe origin point
  camera.position.set(0, 0, 5)

  return camera
}

/**
 * Exports
 * @ignore
 */
export default createCamera
