# threejs-game

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)
[![web](https://img.shields.io/static/v1?logo=icloud&message=Online&label=web&color=success)](https://eternaldeiwos.gitlab.io/threejs-game)

A testbed app demonstrating ThreeJS with Vite

## Table of Contents

- [Install](#install)
- [Usage](#usage)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [License](#license)

## Install

```
yarn
```

## Usage

```
yarn run dev
```

## Maintainers

[@EternalDeiwos](https://github.com/EternalDeiwos)

## Contributing

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## License

MIT © 2022 Greg Linklater
