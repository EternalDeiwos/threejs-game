/**
 * Dependencies
 * @ignore
 */
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls'

/**
 * System
 * @ignore
 */
async function createControls(camera, canvas) {
  const controls = new OrbitControls(camera, canvas)

  controls.enableDamping = true

  controls.tick = () => controls.update()

  return controls
}

/**
 * Exports
 * @ignore
 */
export default createControls
