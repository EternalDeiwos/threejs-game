/**
 * Dependencies
 * @ignore
 */
import Stats from 'stats.js'

/**
 * System
 * @ignore
 */
async function createStats() {
  const stats = new Stats()

  // Show FPS counter
  stats.showPanel(0)

  return stats
}

/**
 * Exports
 * @ignore
 */
export default createStats
