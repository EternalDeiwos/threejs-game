/**
 * Dependencies
 * @ignore
 */
import {
  TextureLoader,
  BoxBufferGeometry,
  MeshStandardMaterial,
  Mesh,
  MathUtils,
} from 'three'

/**
 * Module Dependencies
 * @ignore
 */
import UVTestImage from '../assets/texture/uv-test-bw.jpg'

/**
 * System
 * @ignore
 */
async function createMaterial() {
  const textureLoader = new TextureLoader()

  const texture = await textureLoader.loadAsync(UVTestImage)

  const material = new MeshStandardMaterial({
    // color: 0x00ff00,
    map: texture,
  })

  return material
}

async function createCube() {
  const geometry = new BoxBufferGeometry(2, 2, 2)
  const material = await createMaterial()

  // create a Mesh containing the geometry and material
  const cube = new Mesh(geometry, material)

  const radiansPerSecond = MathUtils.degToRad(20)

  cube.tick = (delta) => {
    cube.rotation.z += delta * radiansPerSecond
    cube.rotation.x += delta * radiansPerSecond
    cube.rotation.y += delta * radiansPerSecond
  }

  return cube
}

/**
 * Exports
 * @ignore
 */
export default createCube
