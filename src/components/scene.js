/**
 * Dependencies
 * @ignore
 */
import { Color, Scene } from 'three'

/**
 * System
 * @ignore
 */
async function createScene() {
  const scene = new Scene()

  scene.background = new Color('skyblue')

  return scene
}

/**
 * Exports
 * @ignore
 */
export default createScene
