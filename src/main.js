/**
 * Dependencies
 * @ignore
 */
import World from './world'

/**
 * Scene
 * @ignore
 */
const world = new World(document.body)
world.init().then(() => {
  world.render()
  world.start()
})
